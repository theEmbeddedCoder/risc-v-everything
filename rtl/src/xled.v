`timescale 1ns / 1ps

module xled (
	input						clk,

	//data interface 
	input						sel,
	input 					data_in,

	output reg				data_out
	
	);


	always @ (posedge clk)
		if (sel)
			data_out <= data_in;


endmodule
