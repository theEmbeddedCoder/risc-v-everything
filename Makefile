SIM_DIR = simulation/icarus
#FPGA_DIR = fpga/xilinx/AES-KU040-DB-G
FPGA_DIR = fpga/xilinx/SP605

sim:
	make -C $(SIM_DIR) 

sim_cnn:
	make -C $(SIM_DIR) cnn_all

fpga:
	make -C $(FPGA_DIR)

fpga_cnn:
	make -C $(FPGA_DIR) top_system_cnn.bit

clean: 
	make -C  $(SIM_DIR) clean
	make -C fpga/xilinx/AES-KU040-DB-G clean
	make -C fpga/xilinx/SP605 clean

.PHONY: sim fpga clean
