#include "cnn_fpga.h"
#include "system.h"
#include "iob-uart.h"

#define UART (UART_BASE<<(DATA_W-N_SLAVES_W))
#define SOFT_RESET (SOFT_RESET_BASE<<(ADDR_W-N_SLAVES_W))


extern int16_t weights_fp[NR_WEIGHT];
extern int16_t image_fp[IMAGE_SIZE];

int16_t outConv_fp[12672];
int16_t outMax_fp[3168];
int16_t outConn_fp[10];
int16_t outSoftMax_fp[10];

/* CUSTOM UART FUNCTIONS TO SP605 */

void my_uart_putc(char c) {
  int i = 0;
  //while(MEMGET(base, UART_WRITE_WAIT));
  while(i < 100) {
    if(!MEMGET(base, UART_WRITE_WAIT)) {
      i++;
    }
    else {
      i = 0;
    }
  }
  
  MEMSET(base, UART_DATA, (int)c);
  
  while(i < 100) {
    if(MEMGET(base, UART_WRITE_WAIT)) {
      i++;
    }
    else {
      i = 0;
    }
  }
}

void my_uart_puts(const char *s) {
  while (*s) my_uart_putc(*s++);
}

void my_uart_printf(const char* fmt, ...) {
  va_list args;
  va_start(args, fmt);

  const char *w = fmt;

  char c;

  static char v_char;
  static char buffer [512];
  static unsigned long v;
  static unsigned long digit;
  static int digit_shift;
  static char hex_a = 'a';

  while ((c = *w++) != '\0') {
    if (c != '%') {
      /* Regular character */
      my_uart_putc(c);
    }
    else {
      /* Format Escape Character */
      if ((c = *w++) != '\0') {
        switch (c) {
        case '%': // %%
          my_uart_putc(c);
          break;
        case 'c': // %c
          v_char = (char) va_arg(args, int);
          my_uart_putc(v_char);
          break;
        case 'X': // %X
          hex_a = 'A';  // Capital "%x"
        case 'x': // %x
          /* Process hexadecimal number format. */
          v = va_arg(args, unsigned long);

          /* If the number value is zero, just print and continue. */
          if (v == 0)
            {
              my_uart_putc('0');
              continue;
            }

          /* Find first non-zero digit. */
          digit_shift = 28;
          while (!(v & (0xF << digit_shift))) {
            digit_shift -= 4;
          }

          /* Print digits. */
          for (; digit_shift >= 0; digit_shift -= 4)
            {
              digit = (v & (0xF << digit_shift)) >> digit_shift;
              if (digit <= 9) {
                c = '0' + digit;
              }
              else {
                c = hex_a + digit - 10;
              }
              my_uart_putc(c);
            }

          /* Reset the A character */
          hex_a = 'a';
          break;
        case 's': // %s
          my_uart_puts(va_arg(args, char *));
          break;
          /* %d: print out an int         */
        case 'd':
          v = va_arg(args, unsigned long);
          itoa(v, buffer, 10);
          my_uart_puts(buffer);
          break;	
        default:
          /* Unsupported format character! */
          break;
        }
      }
      else {
        /* String ends with "...%" */
        /* This should be an error ??? */
        break;
      }
    }
  }
  va_end(args);
}

char my_uart_getc() {
  //while(!MEMGET(base, UART_READ_VALID));

  int i = 0;
  //while(MEMGET(base, UART_WRITE_WAIT));
  while(i < 100) {
    if(MEMGET(base, UART_READ_VALID)) {
      i++;
    }
    else {
      i = 0;
    }
  }


  return( (char) MEMGET(base, UART_DATA));
}


/* CNN CODE */

int load_image(int size_im) {
	int i;
	char aux = -1;

	for (i = 0; i < IMAGE_SIZE; ++i) {
		aux =  my_uart_getc();
		image_fp[i] = aux;
		//my_uart_printf("%d-%d\n", i, image_fp[i]);
	}

	return 0;
}

void displayNumber(int size_im){
	for (int y=0; y<size_im; y++){
		my_uart_printf("| ");
		for (int x=0; x<size_im; x++){
			my_uart_printf("%s", image_fp[y*size_im+x] ? "X" : "." );
		}
		my_uart_printf(" |\n");
	}
}

/* CNN Function */

// The convolution layer consists of 22 feature maps.
// Each feature map has a set of 5*5 weights and a single bias.
void forward_convolutional_layer() {
	int16_t i, j, k, f, row, col;
	int16_t *matB_fp = weights_fp + 22;

	for (f=0; f<22; f++) {

		for (i=0; i<24; i++) {
			for (j=0; j<24; j++) {
				outConv_fp[f*576 + (i*24+j)] = weights_fp[f];
				for (k=0; k<25; k++) {
					row = k / 5;
					col = k % 5;
					outConv_fp[f*576 + (i*24+j)] += image_fp[(i*28+j) + row*28+col] * matB_fp[f*25 + k];
				}
			}
		}	
	}
}

void forward_maxpool_layer() {
	int16_t f, i, j;

	for (f=0; f<22; f++) {
		for (i=0; i<12; i++) {
			for (j=0; j<12; j++) {
				outMax_fp[f*144 + (i*12+j)] = outConv_fp[f*576 + ((i*2)*24+j*2)];
				if(outMax_fp[f*144 + (i*12+j)] < outConv_fp[f*576 + ((i*2)*24+j*2) + 1])
					outMax_fp[f*144 + (i*12+j)] = outConv_fp[f*576 + ((i*2)*24+j*2) + 1];
				if(outMax_fp[f*144 + (i*12+j)] < outConv_fp[f*576 + ((i*2)*24+j*2) + 24])
					outMax_fp[f*144 + (i*12+j)] = outConv_fp[f*576 + ((i*2)*24+j*2) + 24];
				if(outMax_fp[f*144 + (i*12+j)] < outConv_fp[f*576 + ((i*2)*24+j*2) + 25])
					outMax_fp[f*144 + (i*12+j)] = outConv_fp[f*576 + ((i*2)*24+j*2) + 25];
			}
		}
	}
}

void forward_connected_layer() {
    int16_t i, j, k , m;
    int16_t *matW_fp, *mbias_fp;
    int aux;
    // The 10 bias values of this layer are stored after the 22+550 convolutional bias+weigths
    mbias_fp = (int16_t *)weights_fp + 22 + 550;

    // The 10*2880 weights are stored after the 10 bias values
    matW_fp = (int16_t *)weights_fp + 22 + 550 + 10;

	for (i=0; i<10; i++) {
		// Q5.11 -> Q11.5
		outConn_fp[i] = mbias_fp[i] >> 6;
		for (k=0; k<3168; k++) {
			// Q11.21 =  ( Q5.11              				Q5.11 )
			aux = ((((int) matW_fp[i*3168+k]) * (outMax_fp[k])) >> 1); 
			// Q11.5 = Q11.21 -> Q11.5
			outConn_fp[i] += (aux >> 16); 
		}
	}
	
}



// Digit classification is performed using 3 layers:
// 1. Convolutional layer
// 2. Pooling layer
// 3. Fully-connected layer
int predict_mnist() {

	forward_convolutional_layer();
	forward_maxpool_layer();
	forward_connected_layer();	
}

int main(int argc, const char * argv[]) {

	uart_init(UART,UART_CLK_FREQ/UART_BAUD_RATE);   

	my_uart_puts("CNN MNIST!\n");

	/*if(load_weights(NR_WEIGHT) == -1) {
		my_uart_printf("Error loading weights\n");
		return 0;
	}*/

	/*load_image(IMAGE_SIZE);*/

	my_uart_puts("Image Received\n");

	displayNumber(IMAGE_WIDTH);
	while(1) {
		predict_mnist();

		int16_t max = outConn_fp[0];
		int best = 0;
		for (int i = 0; i < 10; ++i) {
			if(outConn_fp[i] > max) {
				max = outConn_fp[i];
				best = i;
			}
		}

		my_uart_printf("Prediction: %d\n", best);
	}
	return 0;
}


