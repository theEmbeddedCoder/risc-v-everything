`timescale 1 ns / 1 ps

`include "system.vh"

module top_system (
               input              clk200,
               input              reset,
               output             trap,

               //UART
               output             uart_txd,
               input              uart_rxd,
               output             uart_rts_b,
               input              uart_cts_b

               //,output led                      
`ifdef DDR //AXI MASTER INTERFACE
               // Address-Write
               , output m_axi_awvalid,
               input              m_axi_awready,
               output [`ADDR_W:0] m_axi_awaddr,
               /// Data-Write
               output             m_axi_wvalid,
               input              m_axi_wready,
               output [`DATA_W:0] m_axi_wdata,
               output [ 3:0]      m_axi_wstrb,
               // Write-Response
               input              m_axi_bvalid,
               output             m_axi_bready,
               // Address-Read
               output             m_axi_arvalid,
               input              m_axi_arready,
               output [`ADDR_W:0] m_axi_araddr,
               output [7:0]       m_axi_arlen,
               output [2:0]       m_axi_arsize,
               output [1:0]       m_axi_arburst,
               // Data-Read
               input              m_axi_rvalid,
               output             m_axi_rready,
               input [`DATA_W:0]  m_axi_rdata,
               input              m_axi_rlast
`endif
               );
					
// New CLOCK AT 50 MHz
reg [1:0] counter_clk;
wire clk;

always @(posedge clk200) begin
	if(reset)
		counter_clk <= 2'd0;
	else begin
		counter_clk <= counter_clk + 2'd1;
	end
end
BUFG clk50 (
	.I(counter_clk[1] & counter_clk[0]),
	.O(clk)
);

wire uart_rts;
wire uart_cts;

assign uart_cts = ~uart_cts_b;

assign uart_rts_b = ~uart_rts;

//initial reset
reg [15:0]                      reset_cnt;
wire                            reset_int = (reset_cnt != 16'hFFFF);

always @(posedge clk, posedge reset)
  if(reset)
	 reset_cnt <= 16'b0;
  else if (reset_cnt != 16'hFFFF)
	 reset_cnt <= reset_cnt+1'b1;
     
					
system system (
               .clk(clk),
               .reset(reset_int),
               .trap(trap),

               //UART
               .uart_txd(uart_txd),
               .uart_rxd(uart_rxd),
               .uart_rts(uart_rts),
               .uart_cts(uart_cts)

               //,.led(led)
                                  
`ifdef DDR //AXI MASTER INTERFACE
               // Address-Write
               , .m_axi_awvalid(m_axi_awvalid),
               .m_axi_awready(m_axi_awready),
               .m_axi_awaddr(m_axi_awaddr),
               /// Data-Write
               .m_axi_wvalid(m_axi_wvalid),
               .m_axi_wready(m_axi_wready),
               .m_axi_wdata(m_axi_wdata),
               .m_axi_wstrb(m_axi_wstrb),
               // Write-Response
               .m_axi_bvalid(m_axi_bvalid),
               .m_axi_bready(m_axi_bready),
               // Address-Read
               .m_axi_arvalid(m_axi_arvalid),
               .m_axi_arready(m_axi_arready),
               .m_axi_araddr(m_axi_araddr),
               .m_axi_arlen(m_axi_arlen),
               .m_axi_arsize(m_axi_arsize),
               .m_axi_arburst(m_axi_arburst),
               // Data-Read
               .m_axi_rvalid(m_axi_rvalid),
               .m_axi_rready(m_axi_rready),
               .m_axi_rdata(m_axi_rdata),
               .m_axi_rlast(m_axi_rlast)
`endif
               );

   
     
endmodule
